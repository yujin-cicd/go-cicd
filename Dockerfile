FROM golang:1.14

ENV GO111MODULE=on

WORKDIR /usr/src/app

COPY . .

RUN apt-get  update && \
    apt-get install git && \
    go get github.com/gorilla/mux && \
    go build -o main ./cmd/main.go
    
CMD [ "/usr/src/app/main" ]

EXPOSE 8080

