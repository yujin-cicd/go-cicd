package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/", indexHandler).Methods("GET")
	router.HandleFunc("/users/{id}", usersHandler).Methods("GET")
	router.HandleFunc("/prods/{id:[0-9]+}", prodsHandler).Methods("GET")
	router.HandleFunc("/zoo", dmHandler).Methods("POST")
	http.Handle("/", router)
	// post
	// application json (body), multipartform (binary file), urlencode, querystring
	// 나는 querystring

	log.Fatal(http.ListenAndServe(":8080", router))
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello world")
}

func usersHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) // router 에 포함된 전체 값을 읽어옴
	id := vars["id"]
	fmt.Fprint(w, "user id : ", id)
}

func prodsHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r) // router 에 포함된 전체 값을 읽어옴
	id := vars["id"]
	fmt.Fprint(w, "user id : ", id)
}

func dmHandler(w http.ResponseWriter, r *http.Request) {

}
